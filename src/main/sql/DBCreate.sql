﻿drop database if exists study00;
create database study00;

\connect study00;

drop table if exists postcode;
drop table if exists city;
drop table if exists region;
drop table if exists country;
drop SEQUENCE city_id_seq;





create table country(
  iso_code char(2) primary key,
  name varchar(60) not null
);


INSERT INTO country (iso_code,name) VALUES ('BD','Bangladesh, People''s Republic of');
INSERT INTO country (iso_code,name) VALUES ('BE','Belgium, Kingdom of');
INSERT INTO country (iso_code,name) VALUES ('BF','Burkina Faso');
INSERT INTO country (iso_code,name) VALUES ('BG','Bulgaria, People''s Republic of');
INSERT INTO country (iso_code,name) VALUES ('BA','Bosnia and Herzegovina');
INSERT INTO country (iso_code,name) VALUES ('BB','Barbados');
INSERT INTO country (iso_code,name) VALUES ('WF','Wallis and Futuna Islands');
INSERT INTO country (iso_code,name) VALUES ('BM','Bermuda');
INSERT INTO country (iso_code,name) VALUES ('BN','Brunei Darussalam');
INSERT INTO country (iso_code,name) VALUES ('BO','Bolivia, Republic of');
INSERT INTO country (iso_code,name) VALUES ('BH','Bahrain, Kingdom of');
INSERT INTO country (iso_code,name) VALUES ('BI','Burundi, Republic of');
INSERT INTO country (iso_code,name) VALUES ('BJ','Benin, People''s Republic of');
INSERT INTO country (iso_code,name) VALUES ('BT','Bhutan, Kingdom of');
INSERT INTO country (iso_code,name) VALUES ('JM','Jamaica');
INSERT INTO country (iso_code,name) VALUES ('BW','Botswana, Republic of');
INSERT INTO country (iso_code,name) VALUES ('WS','Samoa, Independent State of');
INSERT INTO country (iso_code,name) VALUES ('BS','Bahamas, Commonwealth of the');
INSERT INTO country (iso_code,name) VALUES ('BY','Belarus');
INSERT INTO country (iso_code,name) VALUES ('BZ','Belize');
INSERT INTO country (iso_code,name) VALUES ('TN','Tunisia, Republic of');
INSERT INTO country (iso_code,name) VALUES ('RW','Rwanda, Rwandese Republic');
INSERT INTO country (iso_code,name) VALUES ('TL','Timor-Leste, Democratic Republic of');
INSERT INTO country (iso_code,name) VALUES ('RE','Reunion');
INSERT INTO country (iso_code,name) VALUES ('TM','Turkmenistan');
INSERT INTO country (iso_code,name) VALUES ('TJ','Tajikistan');
INSERT INTO country (iso_code,name) VALUES ('RO','Romania, Socialist Republic of');
INSERT INTO country (iso_code,name) VALUES ('TK','Tokelau (Tokelau Islands)');
INSERT INTO country (iso_code,name) VALUES ('GW','Guinea-Bissau, Republic of');
INSERT INTO country (iso_code,name) VALUES ('GU','Guam');
INSERT INTO country (iso_code,name) VALUES ('GT','Guatemala, Republic of');
INSERT INTO country (iso_code,name) VALUES ('GS','South Georgia and the South Sandwich Islands');
INSERT INTO country (iso_code,name) VALUES ('GR','Greece, Hellenic Republic');
INSERT INTO country (iso_code,name) VALUES ('GQ','Equatorial Guinea, Republic of');
INSERT INTO country (iso_code,name) VALUES ('GP','Guadaloupe');
INSERT INTO country (iso_code,name) VALUES ('JP','Japan');
INSERT INTO country (iso_code,name) VALUES ('GY','Guyana, Republic of');
INSERT INTO country (iso_code,name) VALUES ('GF','French Guiana');
INSERT INTO country (iso_code,name) VALUES ('GE','Georgia');
INSERT INTO country (iso_code,name) VALUES ('GD','Grenada');
INSERT INTO country (iso_code,name) VALUES ('GB','United Kingdom of Great Britain & N. Ireland');
INSERT INTO country (iso_code,name) VALUES ('GA','Gabon, Gabonese Republic');
INSERT INTO country (iso_code,name) VALUES ('SV','El Salvador, Republic of');
INSERT INTO country (iso_code,name) VALUES ('GN','Guinea, Revolutionary People''s Rep''c of');
INSERT INTO country (iso_code,name) VALUES ('GM','Gambia, Republic of the');
INSERT INTO country (iso_code,name) VALUES ('GI','Gibraltar');
INSERT INTO country (iso_code,name) VALUES ('GH','Ghana, Republic of');
INSERT INTO country (iso_code,name) VALUES ('OM','Oman, Sultanate of');
INSERT INTO country (iso_code,name) VALUES ('JO','Jordan, Hashemite Kingdom of');
INSERT INTO country (iso_code,name) VALUES ('HR','Hrvatska (Croatia)');
INSERT INTO country (iso_code,name) VALUES ('HT','Haiti, Republic of');
INSERT INTO country (iso_code,name) VALUES ('HU','Hungary, Hungarian People''s Republic');
INSERT INTO country (iso_code,name) VALUES ('HK','Hong Kong, Special Administrative Region of China');
INSERT INTO country (iso_code,name) VALUES ('HN','Honduras, Republic of');
INSERT INTO country (iso_code,name) VALUES ('VE','Venezuela, Bolivarian Republic of');
INSERT INTO country (iso_code,name) VALUES ('PR','Puerto Rico');
INSERT INTO country (iso_code,name) VALUES ('PS','Palestinian Territory, Occupied');
INSERT INTO country (iso_code,name) VALUES ('PW','Palau');
INSERT INTO country (iso_code,name) VALUES ('PY','Paraguay, Republic of');
INSERT INTO country (iso_code,name) VALUES ('PA','Panama, Republic of');
INSERT INTO country (iso_code,name) VALUES ('PG','Papua New Guinea');
INSERT INTO country (iso_code,name) VALUES ('PE','Peru, Republic of');
INSERT INTO country (iso_code,name) VALUES ('PK','Pakistan, Islamic Republic of');
INSERT INTO country (iso_code,name) VALUES ('PH','Philippines, Republic of the');
INSERT INTO country (iso_code,name) VALUES ('PN','Pitcairn Island');
INSERT INTO country (iso_code,name) VALUES ('PL','Poland, Polish People''s Republic');
INSERT INTO country (iso_code,name) VALUES ('PM','St. Pierre and Miquelon');
INSERT INTO country (iso_code,name) VALUES ('ZM','Zambia, Republic of');
INSERT INTO country (iso_code,name) VALUES ('EH','Western Sahara');
INSERT INTO country (iso_code,name) VALUES ('EE','Estonia');
INSERT INTO country (iso_code,name) VALUES ('EG','Egypt, Arab Republic of');
INSERT INTO country (iso_code,name) VALUES ('ZA','South Africa, Republic of');
INSERT INTO country (iso_code,name) VALUES ('IT','Italy, Italian Republic');
INSERT INTO country (iso_code,name) VALUES ('VN','Viet Nam, Socialist Republic of');
INSERT INTO country (iso_code,name) VALUES ('ET','Ethiopia');
INSERT INTO country (iso_code,name) VALUES ('SO','Somalia, Somali Republic');
INSERT INTO country (iso_code,name) VALUES ('SA','Saudi Arabia, Kingdom of');
INSERT INTO country (iso_code,name) VALUES ('ER','Eritrea');
INSERT INTO country (iso_code,name) VALUES ('MD','Moldova, Republic of');
INSERT INTO country (iso_code,name) VALUES ('MG','Madagascar, Republic of');
INSERT INTO country (iso_code,name) VALUES ('MA','Morocco, Kingdom of');
INSERT INTO country (iso_code,name) VALUES ('MC','Monaco, Principality of');
INSERT INTO country (iso_code,name) VALUES ('ML','Mali, Republic of');
INSERT INTO country (iso_code,name) VALUES ('MO','Macao, Special Administrative Region of China');
INSERT INTO country (iso_code,name) VALUES ('MK','Macedonia, the former Yugoslav Republic of');
INSERT INTO country (iso_code,name) VALUES ('MU','Mauritius');
INSERT INTO country (iso_code,name) VALUES ('MT','Malta, Republic of');
INSERT INTO country (iso_code,name) VALUES ('MW','Malawi, Republic of');
INSERT INTO country (iso_code,name) VALUES ('MV','Maldives, Republic of');
INSERT INTO country (iso_code,name) VALUES ('MQ','Martinique');
INSERT INTO country (iso_code,name) VALUES ('MP','Northern Mariana Islands');
INSERT INTO country (iso_code,name) VALUES ('MS','Montserrat');
INSERT INTO country (iso_code,name) VALUES ('MR','Mauritania, Islamic Republic of');
INSERT INTO country (iso_code,name) VALUES ('UG','Uganda, Republic of');
INSERT INTO country (iso_code,name) VALUES ('IL','Israel, State of');
INSERT INTO country (iso_code,name) VALUES ('FR','France, French Republic');
INSERT INTO country (iso_code,name) VALUES ('AW','Aruba');
INSERT INTO country (iso_code,name) VALUES ('SH','St. Helena');
INSERT INTO country (iso_code,name) VALUES ('FI','Finland, Republic of');
INSERT INTO country (iso_code,name) VALUES ('FJ','Fiji, Republic of the Fiji Islands');
INSERT INTO country (iso_code,name) VALUES ('FK','Falkland Islands (Malvinas)');
INSERT INTO country (iso_code,name) VALUES ('FO','Faeroe Islands');
INSERT INTO country (iso_code,name) VALUES ('NI','Nicaragua, Republic of');
INSERT INTO country (iso_code,name) VALUES ('NL','Netherlands, Kingdom of the');
INSERT INTO country (iso_code,name) VALUES ('NO','Norway, Kingdom of');
INSERT INTO country (iso_code,name) VALUES ('NA','Namibia');
INSERT INTO country (iso_code,name) VALUES ('VU','Vanuatu');
INSERT INTO country (iso_code,name) VALUES ('NC','New Caledonia');
INSERT INTO country (iso_code,name) VALUES ('NE','Niger, Republic of the');
INSERT INTO country (iso_code,name) VALUES ('NF','Norfolk Island');
INSERT INTO country (iso_code,name) VALUES ('NG','Nigeria, Federal Republic of');
INSERT INTO country (iso_code,name) VALUES ('NP','Nepal, Kingdom of');
INSERT INTO country (iso_code,name) VALUES ('NR','Nauru, Republic of');
INSERT INTO country (iso_code,name) VALUES ('NU','Niue, Republic of');
INSERT INTO country (iso_code,name) VALUES ('CK','Cook Islands');
INSERT INTO country (iso_code,name) VALUES ('CH','Switzerland, Swiss Confederation');
INSERT INTO country (iso_code,name) VALUES ('CO','Colombia, Republic of');
INSERT INTO country (iso_code,name) VALUES ('CM','Cameroon, United Republic of');
INSERT INTO country (iso_code,name) VALUES ('CC','Cocos (Keeling) Islands');
INSERT INTO country (iso_code,name) VALUES ('CG','Congo, People''s Republic of');
INSERT INTO country (iso_code,name) VALUES ('CF','Central African Republic');
INSERT INTO country (iso_code,name) VALUES ('CZ','Czech Republic');
INSERT INTO country (iso_code,name) VALUES ('CY','Cyprus, Republic of');
INSERT INTO country (iso_code,name) VALUES ('CX','Christmas Island');
INSERT INTO country (iso_code,name) VALUES ('CR','Costa Rica, Republic of');
INSERT INTO country (iso_code,name) VALUES ('CV','Cape Verde, Republic of');
INSERT INTO country (iso_code,name) VALUES ('SZ','Swaziland, Kingdom of');
INSERT INTO country (iso_code,name) VALUES ('KG','Kyrgyz Republic');
INSERT INTO country (iso_code,name) VALUES ('KE','Kenya, Republic of');
INSERT INTO country (iso_code,name) VALUES ('SR','Suriname, Republic of');
INSERT INTO country (iso_code,name) VALUES ('KH','Cambodia, Kingdom of');
INSERT INTO country (iso_code,name) VALUES ('KN','St. Kitts and Nevis');
INSERT INTO country (iso_code,name) VALUES ('KM','Comoros, Union of the');
INSERT INTO country (iso_code,name) VALUES ('ST','Sao Tome and Principe, Democratic Republic of');
INSERT INTO country (iso_code,name) VALUES ('SK','Slovakia (Slovak Republic)');
INSERT INTO country (iso_code,name) VALUES ('KR','Korea, Republic of');
INSERT INTO country (iso_code,name) VALUES ('SI','Slovenia');
INSERT INTO country (iso_code,name) VALUES ('KW','Kuwait, State of');
INSERT INTO country (iso_code,name) VALUES ('SN','Senegal, Republic of');
INSERT INTO country (iso_code,name) VALUES ('SM','San Marino, Republic of');
INSERT INTO country (iso_code,name) VALUES ('SL','Sierra Leone, Republic of');
INSERT INTO country (iso_code,name) VALUES ('SC','Seychelles, Republic of');
INSERT INTO country (iso_code,name) VALUES ('SB','Solomon Islands');
INSERT INTO country (iso_code,name) VALUES ('KY','Cayman Islands');
INSERT INTO country (iso_code,name) VALUES ('SG','Singapore, Republic of');
INSERT INTO country (iso_code,name) VALUES ('SE','Sweden, Kingdom of');
INSERT INTO country (iso_code,name) VALUES ('DO','Dominican Republic');
INSERT INTO country (iso_code,name) VALUES ('DM','Dominica, Commonwealth of');
INSERT INTO country (iso_code,name) VALUES ('DJ','Djibouti, Republic of');
INSERT INTO country (iso_code,name) VALUES ('DK','Denmark, Kingdom of');
INSERT INTO country (iso_code,name) VALUES ('VG','British Virgin Islands');
INSERT INTO country (iso_code,name) VALUES ('DE','Germany');
INSERT INTO country (iso_code,name) VALUES ('YE','Yemen');
INSERT INTO country (iso_code,name) VALUES ('DZ','Algeria, People''s Democratic Republic of');
INSERT INTO country (iso_code,name) VALUES ('UY','Uruguay, Eastern Republic of');
INSERT INTO country (iso_code,name) VALUES ('YT','Mayotte');
INSERT INTO country (iso_code,name) VALUES ('LB','Lebanon, Lebanese Republic');
INSERT INTO country (iso_code,name) VALUES ('LC','St. Lucia');
INSERT INTO country (iso_code,name) VALUES ('LA','Lao People''s Democratic Republic');
INSERT INTO country (iso_code,name) VALUES ('TV','Tuvalu');
INSERT INTO country (iso_code,name) VALUES ('TW','Taiwan, Province of China');
INSERT INTO country (iso_code,name) VALUES ('TT','Trinidad and Tobago, Republic of');
INSERT INTO country (iso_code,name) VALUES ('TR','Turkey, Republic of');
INSERT INTO country (iso_code,name) VALUES ('LK','Sri Lanka, Democratic Socialist Republic of');
INSERT INTO country (iso_code,name) VALUES ('LI','Liechtenstein, Principality of');
INSERT INTO country (iso_code,name) VALUES ('LV','Latvia');
INSERT INTO country (iso_code,name) VALUES ('TO','Tonga, Kingdom of');
INSERT INTO country (iso_code,name) VALUES ('LT','Lithuania');
INSERT INTO country (iso_code,name) VALUES ('LU','Luxembourg, Grand Duchy of');
INSERT INTO country (iso_code,name) VALUES ('LS','Lesotho, Kingdom of');
INSERT INTO country (iso_code,name) VALUES ('TH','Thailand, Kingdom of');
INSERT INTO country (iso_code,name) VALUES ('TF','French Southern Territories');
INSERT INTO country (iso_code,name) VALUES ('TG','Togo, Togolese Republic');
INSERT INTO country (iso_code,name) VALUES ('TD','Chad, Republic of');
INSERT INTO country (iso_code,name) VALUES ('TC','Turks and Caicos Islands');
INSERT INTO country (iso_code,name) VALUES ('VA','Holy See (Vatican City State)');
INSERT INTO country (iso_code,name) VALUES ('VC','St. Vincent and the Grenadines');
INSERT INTO country (iso_code,name) VALUES ('AE','United Arab Emirates');
INSERT INTO country (iso_code,name) VALUES ('AD','Andorra, Principality of');
INSERT INTO country (iso_code,name) VALUES ('AG','Antigua and Barbuda');
INSERT INTO country (iso_code,name) VALUES ('AF','Afghanistan');
INSERT INTO country (iso_code,name) VALUES ('AI','Anguilla');
INSERT INTO country (iso_code,name) VALUES ('VI','US Virgin Islands');
INSERT INTO country (iso_code,name) VALUES ('IS','Iceland, Republic of');
INSERT INTO country (iso_code,name) VALUES ('AM','Armenia');
INSERT INTO country (iso_code,name) VALUES ('AL','Albania, People''s Socialist Republic of');
INSERT INTO country (iso_code,name) VALUES ('AO','Angola, Republic of');
INSERT INTO country (iso_code,name) VALUES ('AN','Netherlands Antilles');
INSERT INTO country (iso_code,name) VALUES ('AS','American Samoa');
INSERT INTO country (iso_code,name) VALUES ('AT','Austria, Republic of');
INSERT INTO country (iso_code,name) VALUES ('IO','British Indian Ocean Territory (Chagos Archipelago)');
INSERT INTO country (iso_code,name) VALUES ('IN','India, Republic of');
INSERT INTO country (iso_code,name) VALUES ('TZ','Tanzania, United Republic of');
INSERT INTO country (iso_code,name) VALUES ('AZ','Azerbaijan, Republic of');
INSERT INTO country (iso_code,name) VALUES ('IE','Ireland');
INSERT INTO country (iso_code,name) VALUES ('QA','Qatar, State of');
INSERT INTO country (iso_code,name) VALUES ('MZ','Mozambique, People''s Republic of');
INSERT INTO country (iso_code,name) VALUES ('CS','Serbia and Montenegro');
INSERT INTO country (iso_code,name) VALUES ('BR','Brazil, Federative Republic of');
INSERT INTO country (iso_code,name) VALUES ('FM','Micronesia, Federated States of');
INSERT INTO country (iso_code,name) VALUES ('PT','Portugal, Portuguese Republic');
INSERT INTO country (iso_code,name) VALUES ('NZ','New Zealand');
INSERT INTO country (iso_code,name) VALUES ('UA','Ukraine');
INSERT INTO country (iso_code,name) VALUES ('MN','Mongolia, Mongolian People''s Republic');
INSERT INTO country (iso_code,name) VALUES ('PF','French Polynesia');
INSERT INTO country (iso_code,name) VALUES ('CL','Chile, Republic of');
INSERT INTO country (iso_code,name) VALUES ('UM','United States Minor Outlying Islands');
INSERT INTO country (iso_code,name) VALUES ('SJ','Svalbard & Jan Mayen Islands');
INSERT INTO country (iso_code,name) VALUES ('KZ','Kazakhstan, Republic of');
INSERT INTO country (iso_code,name) VALUES ('MY','Malaysia');
INSERT INTO country (iso_code,name) VALUES ('EC','Ecuador, Republic of');
INSERT INTO country (iso_code,name) VALUES ('CD','Congo, Democratic Republic of');
INSERT INTO country (iso_code,name) VALUES ('GL','Greenland');
INSERT INTO country (iso_code,name) VALUES ('ID','Indonesia, Republic of');
INSERT INTO country (iso_code,name) VALUES ('ES','Spain, Spanish State');
INSERT INTO country (iso_code,name) VALUES ('UZ','Uzbekistan');
INSERT INTO country (iso_code,name) VALUES ('KI','Kiribati, Republic of');
INSERT INTO country (iso_code,name) VALUES ('MH','Marshall Islands');
INSERT INTO country (iso_code,name) VALUES ('RU','Russian Federation');
INSERT INTO country (iso_code,name) VALUES ('CN','China, People''s Republic of');
INSERT INTO country (iso_code,name) VALUES ('CA','Canada');
INSERT INTO country (iso_code,name) VALUES ('AQ','Antarctica (the territory South of 60 deg S)');
INSERT INTO country (iso_code,name) VALUES ('AR','Argentina, Argentine Republic');
INSERT INTO country (iso_code,name) VALUES ('AU','Australia, Commonwealth of');
INSERT INTO country (iso_code,name) VALUES ('US','United States of America');
INSERT INTO country (iso_code,name) VALUES ('MX','Mexico, United Mexican States');
INSERT INTO country (iso_code,name) VALUES ('BV','Bouvet Island (Bouvetoya)');
INSERT INTO country (iso_code,name) VALUES ('HM','Heard and McDonald Islands');
INSERT INTO country (iso_code,name) VALUES ('AX','Aland Islands');
INSERT INTO country (iso_code,name) VALUES ('BL','Saint Barthélemy');
INSERT INTO country (iso_code,name) VALUES ('BQ','Bonaire, Saint Eustatius and Saba');
INSERT INTO country (iso_code,name) VALUES ('CW','Curaçao');
INSERT INTO country (iso_code,name) VALUES ('GG','Guernsey');
INSERT INTO country (iso_code,name) VALUES ('IM','Isle of Man');
INSERT INTO country (iso_code,name) VALUES ('JE','Jersey');
INSERT INTO country (iso_code,name) VALUES ('XK','Kosovo');
INSERT INTO country (iso_code,name) VALUES ('ME','Montenegro');
INSERT INTO country (iso_code,name) VALUES ('MF','Saint Martin');
INSERT INTO country (iso_code,name) VALUES ('RS','Serbia');
INSERT INTO country (iso_code,name) VALUES ('SS','South Sudan');
INSERT INTO country (iso_code,name) VALUES ('SX','Sint Maarten');
INSERT INTO country (iso_code,name) VALUES ('A1','Anonymous Proxy');
INSERT INTO country (iso_code,name) VALUES ('A2','Satellite Provider');
INSERT INTO country (iso_code,name) VALUES ('O1','Other Country');
INSERT INTO country (iso_code,name) VALUES ('AP','Asia/Pacific Region');
INSERT INTO country (iso_code,name) VALUES ('CI','Ivory Coast');
INSERT INTO country (iso_code,name) VALUES ('CU','Cuba');
INSERT INTO country (iso_code,name) VALUES ('IQ','Iraq');
INSERT INTO country (iso_code,name) VALUES ('IR','Iran');
INSERT INTO country (iso_code,name) VALUES ('KP','North Korea');
INSERT INTO country (iso_code,name) VALUES ('LR','Liberia');
INSERT INTO country (iso_code,name) VALUES ('LY','Libya');
INSERT INTO country (iso_code,name) VALUES ('MM','Myanmar');
INSERT INTO country (iso_code,name) VALUES ('SD','Sudan');
INSERT INTO country (iso_code,name) VALUES ('SY','Syria');
INSERT INTO country (iso_code,name) VALUES ('ZW','Zimbabwe');
INSERT INTO country (iso_code,name) VALUES ('nn','none');




create table  region(
  code char(3) not null primary key,
  name varchar(30) not null,
  country_code char(2) not null,
  foreign key (country_code) references country(iso_code)
);


create table postcode(
  code char(4) not null primary key,
  region_code varchar(3),
  foreign key (region_code) references region(code)
);

CREATE SEQUENCE city_id_seq;
create table city(
  id int NOT NULL DEFAULT nextval('city_id_seq'),
  region_code varchar(3) not null,
  name varchar(60) not null,
  latitude  float(7) not null,
  longitude  float(7) not null,
  PRIMARY KEY (name, latitude, longitude),
  foreign key (region_code) references region(code)
);




