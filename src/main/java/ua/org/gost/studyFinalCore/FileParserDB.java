package main.java.ua.org.gost.studyFinalCore;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileParserDB {

	public DBConnection dbc = new DBConnection();
	private ArrayList<String> fileLines = new ArrayList<>();
	private int lineSplitLenght;
	private HashSet<String> uniqueRegionRows = new HashSet<>();
	private HashSet<String> uniquePostcodeRows = new HashSet<>();
	public HashSet<String> cityPrimaryKeys = new HashSet<>();
	public HashSet<String> postPrimaryKeys = new HashSet<>();
	public HashSet<String> regionPrimaryKeys = new HashSet<>();
	public PreparedStatement pstmtInsertRegion;
	public PreparedStatement pstmtUpdateRegion;
	public PreparedStatement pstmtInsertPostcode;
	public PreparedStatement pstmtUpdatePostcode;
	public PreparedStatement pstmtInsertCity;
	public PreparedStatement pstmtUpdateCity;
	public Statement stCity = null;
	public Statement stPostcode = null;
	public Statement stRegion = null;
	private HashSet<String> cityDuplicateCheck = new HashSet<>();

	private void statemenstInitialize() {

		try {

			pstmtInsertRegion = dbc.connection
					.prepareStatement("INSERT INTO region"
							+ " (code, name, country_code) VALUES (?, ?, ?)");

			pstmtUpdateRegion = dbc.connection
					.prepareStatement("UPDATE region SET"
							+ " name = ?, country_code = ? WHERE code = ?");

			pstmtInsertPostcode = dbc.connection
					.prepareStatement("INSERT INTO postcode"
							+ " (code, region_code) VALUES (?, ?)");

			pstmtUpdatePostcode = dbc.connection
					.prepareStatement("UPDATE postcode SET"
							+ " region_code = ? WHERE code = ?");

			pstmtInsertCity = dbc.connection
					.prepareStatement("INSERT INTO city"
							+ " (region_code, name, latitude, longitude) VALUES (?, ?, ?, ?)");

			pstmtUpdateCity = dbc.connection
					.prepareStatement("UPDATE city SET"
							+ " region_code = ? WHERE name = ? AND latitude = ? AND longitude = ?");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public FileParserDB(File file) {
		getLines(file);
		dbc.driverConnection();
		statemenstInitialize();
		getPKCity();
		getPKPostcode();
		getPKRegion();
	}

	public void allAtOnce() {
		String[] splitSt;
		for (String s : fileLines) {
			splitSt = s.split("\t");
			lineSplitLenght = splitSt.length;
			regionUpdate(splitSt[4], splitSt[3], splitSt[0]);
			cityUpdate(splitSt[4], splitSt[2],
					(splitSt.length == 12) ? splitSt[9] : "0",
					(splitSt.length == 12) ? splitSt[10] : "0");
			postcodeUpdate(splitSt[1], splitSt[4]);
		}
	}

	public int getLines(File file) {
		int linesCount = 0;
		FileReader fr = null;
		String line = null;
		BufferedReader br;

		try {
			fr = new FileReader(file);
		} catch (FileNotFoundException e) {
			System.out.println("No file with DB!");
			e.printStackTrace();
			System.exit(0);
		}

		br = new BufferedReader(fr);

		try {
			while ((line = br.readLine()) != null) {
				linesCount++;
				if (line.split("\t")[0].length() == 2
						&& line.split("\t")[1].length() <= 4
						&& line.split("\t")[1].length() >= 2) {
					fileLines.add(line);

				} else {
					System.out.println("Line " + linesCount
							+ " is not correct!");
				}

			}
			br.close();
		} catch (IOException e) {
			System.out.println("Can't read file with DB!");
			e.printStackTrace();
			System.exit(0);
		}
		System.out.println("Totall lines in file " + fileLines.size() + "\n");
		return fileLines.size();
	}

	public void regionUpdate(String code, String name, String countryCode) {

		int uniqueRegionRowsSize;

		uniqueRegionRowsSize = uniqueRegionRows.size();
		if (lineSplitLenght == 12 && !(code.equals(""))) {
			uniqueRegionRows.add(code + "*" + name + "*" + countryCode);
		} else {
			uniqueRegionRows.add("non*none*GB");
			code = "non";
			name = "none";
		}

		if (uniqueRegionRowsSize - uniqueRegionRows.size() == -1) {
			try {
				if (regionPrimaryKeys.contains(code)) {
					pstmtUpdateRegion.setString(1, name);
					pstmtUpdateRegion.setString(2, countryCode);
					pstmtUpdateRegion.setString(3, code);
					pstmtUpdateRegion.executeUpdate();
				} else {
					pstmtInsertRegion.setString(1, code);
					pstmtInsertRegion.setString(2, name);
					pstmtInsertRegion.setString(3, countryCode);
					pstmtInsertRegion.executeUpdate();
				}
			} catch (SQLException e) {
				System.out.println("regionUpdate executeUptate fail");
				e.printStackTrace();
			}
		}

	}

	public void postcodeUpdate(String postCode, String region) {

		int uniquePostcodeRowsSize;

		uniquePostcodeRowsSize = uniquePostcodeRows.size();
		uniquePostcodeRows.add(postCode + "*" + region);

		if (uniquePostcodeRowsSize - uniquePostcodeRows.size() == -1) {
			try {
				if (region.equals("")) {
					region = "non";
				}
				if (postPrimaryKeys.contains(postCode)) {
					pstmtUpdatePostcode.setString(1, region);
					pstmtUpdatePostcode.setString(2, postCode);
					pstmtUpdatePostcode.executeUpdate();
				} else {
					pstmtInsertPostcode.setString(1, postCode);
					pstmtInsertPostcode.setString(2, region);
					pstmtInsertPostcode.executeUpdate();
				}
			} catch (SQLException e) {
				System.out.println("postcodeUpdate executeUptate fail ");
				e.printStackTrace();
			}
		}

	}

	public void cityUpdate(String region_code, String name, String latitude,
			String longitude) {

		if (!cityDuplicateCheck.contains(name + latitude + longitude)) {
			if (cityPrimaryKeys.contains(name.replace("'", "\"")
					+ Float.valueOf(latitude) + Float.valueOf(longitude))) {
				try {
					pstmtUpdateCity.setString(1, (region_code.equals("") ? "non" : region_code));
					pstmtUpdateCity.setString(2, name.replace("'", "\""));
					pstmtUpdateCity.setFloat(3, Float.valueOf(latitude));
					pstmtUpdateCity.setFloat(4, Float.valueOf(longitude));
					pstmtUpdateCity.executeUpdate();
				} catch (SQLException e) {
					System.out.println("cityUpdate Uptate fail in line " + name
							+ " " + latitude + " " + longitude);
					e.printStackTrace();
				}
			} else {
				try {
					pstmtInsertCity.setString(1, (region_code.equals("") ? "non" : region_code));
					pstmtInsertCity.setString(2, name.replace("'", "\""));
					pstmtInsertCity.setFloat(3, Float.valueOf(latitude));
					pstmtInsertCity.setFloat(4, Float.valueOf(longitude));
					pstmtInsertCity.executeUpdate();
				} catch (SQLException e) {
					System.out.println("cityUpdate Insert fail in line " + name
							+ " " + latitude + " " + longitude);
					e.printStackTrace();
				}
			}
		}

		cityDuplicateCheck.add(name + latitude + longitude);
	}

	private void getPKPostcode() {

		final String QUERY_POSTCODE = "select code from postcode";
		try {
			stPostcode = dbc.connection.createStatement();
			ResultSet rsPostcode = stPostcode.executeQuery(QUERY_POSTCODE);

			while (rsPostcode.next()) {
				postPrimaryKeys.add(rsPostcode.getString(1).replace(" ", ""));
			}
		} catch (SQLException e) {
			System.out.println("PKPostcode error!\n");
			e.printStackTrace();
		}
	}

	private void getPKCity() {

		final String QUERY_CITY = "select name, latitude, longitude from city";
		try {
			stCity = dbc.connection.createStatement();
			ResultSet rsCity = stCity.executeQuery(QUERY_CITY);
			while (rsCity.next()) {
				cityPrimaryKeys.add(rsCity.getString("name")
						+ rsCity.getFloat("latitude")
						+ rsCity.getFloat("longitude"));
			}
		} catch (SQLException e) {
			System.out.println("PKCity error!\n");
			e.printStackTrace();
		}
	}

	private void getPKRegion() {

		final String QUERY_REGION = "select code from region";
		try {
			stRegion = dbc.connection.createStatement();
			ResultSet rsRegion = stRegion.executeQuery(QUERY_REGION);

			while (rsRegion.next()) {
				regionPrimaryKeys.add(rsRegion.getString(1));
			}
		} catch (SQLException e) {
			System.out.println("PKRegion error!\n");
			e.printStackTrace();
		}
	}
}
