package main.java.ua.org.gost.studyFinalCore;

import java.io.File;
import java.sql.SQLException;

public class Main {

	public static void main(String[] args) {
		FileParserDB fpDB = new FileParserDB(new File(args[0]));
		System.out.println("WAIT PLEASE!");
		fpDB.allAtOnce();

		try {
			fpDB.stCity.close();
			fpDB.stPostcode.close();
			fpDB.stRegion.close();
			fpDB.dbc.connection.close();
		} catch (SQLException e) {
			System.out.println("Can't close main connection");
			e.printStackTrace();
		}
		System.out.println("\nProcess DONE! Check yours DB.");
	}
}
