package main.java.ua.org.gost.studyFinalCore;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection {

	public String URL;
	public String USER_NAME;
	public String PASS;
	public String DRIVER;
	private File configFile = new File((new File("").getAbsolutePath())
			+ "\\src\\main\\sql\\DBProperties.cfg");
	public Connection connection = null;
	public Properties prop = new Properties();

	public void getDBFileConfig(File file) {

		FileReader fr = null;
		BufferedReader br;

		try {
			fr = new FileReader(file);
		} catch (FileNotFoundException e) {
			System.out.println("DBProperties.cfg not found!");
			e.printStackTrace();
			System.exit(0);
		}

		br = new BufferedReader(fr);

		try {
			prop.load(br);
		} catch (IOException e1) {
			System.out.println("Can't read cfg file!");
			e1.printStackTrace();
			System.exit(0);
		}

		USER_NAME = prop.getProperty("Database.Prop.user");
		URL = prop.getProperty("Database.DataURL");
		PASS = prop.getProperty("Database.Prop.password");
		DRIVER = prop.getProperty("Database.Driver");

		System.out.println("Config file contains: \nURL = " + URL
				+ "\nUser Name = " + USER_NAME + "\nPassword = " + PASS
				+ "\n\n");
	}

	public void driverConnection() {
		getDBFileConfig(configFile);

		try {
			Class.forName(DRIVER);
			System.out.println("Driver found!");
		} catch (ClassNotFoundException e) {
			System.out.println("Driver not found!");
			e.printStackTrace();
			System.exit(0);
		}

		try {
			connection = DriverManager.getConnection(URL, USER_NAME, PASS);
		} catch (SQLException e) {
			System.out.println("Connection Failed!");
			e.printStackTrace();
			System.exit(0);
		}

		if (connection != null) {
			System.out.println("Connection confirmed!");
		} else {
			System.out.println("Failed to connect!");
			System.exit(0);
		}
	}

}
